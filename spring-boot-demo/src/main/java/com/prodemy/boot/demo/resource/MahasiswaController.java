/**
 * 
 */
package com.prodemy.boot.demo.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prodemy.boot.demo.model.Mahasiswa;
import com.prodemy.boot.demo.repository.MahasiswaRepository;

/**
 * @author wyant
 *
 */
@RestController
@RequestMapping("/mhs")
public class MahasiswaController {
	@Autowired private MahasiswaRepository repo;

	@RequestMapping("/{id}")
	public Mahasiswa findById(@PathVariable("id") String id) {
		return repo.findById(id).get();
	}

	@RequestMapping("/byfak/{kdfak}")
	public List<Mahasiswa> findByKodeFakultas(@PathVariable("kdfak") String kodeFakultas) {
		return repo.findByKodeFakultas(kodeFakultas);
	}
}
