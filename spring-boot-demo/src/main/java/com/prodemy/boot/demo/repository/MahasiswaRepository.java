/**
 * 
 */
package com.prodemy.boot.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.prodemy.boot.demo.model.Mahasiswa;

/**
 * @author wyant
 *
 */
@Repository
public interface MahasiswaRepository extends CrudRepository<Mahasiswa, String> {
	public List<Mahasiswa> findByKodeFakultas(String kodeFakultas);
}
